<?php
$studentId = $_GET['studentId'];
$groupId = $_GET['groupId'];

if (empty($studentId) || empty($groupId)) {
    die('set studentId and groupId params');
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>WebSockets - Simple chat</title>
    <style>
        * { font-family:tahoma; font-size:12px; padding:0px;margin:0px;}
        p { line-height:18px; }
        div { width:500px; margin-left:auto; margin-right:auto;}
        #content { display:none;padding:5px; background:#ddd; border-radius:5px;
            overflow-y: scroll; border:1px solid #CCC;
            margin-top:10px; height: 160px; }
        #input { border-radius:2px; border:1px solid #ccc;
            margin-top:10px; padding:5px; width:400px;
        }
        #status { width:88px;display:block;float:left;margin-top:15px; }

        .topBar {
            display: block;
            padding: 10px;
            border: 1px solid black;
        }

        .btn {
            height: 30px;
            width: 30px;
            border: 1px solid black;
            float: right;
            text-align: center;
            line-height: 30px;
        }

        .btn.unread {
            background-color: red;
        }

        .messageBox {
            border: 1px solid black;
            width: 500px;
            padding: 10px;
            position: absolute;
            right: 0;
            top: 0;
            display: none;

        }

        .show {
            display: block;
        }

        .message {
            border: 1px solid black;
            display: block;
            padding: 10px;

        }
    </style>
</head>
<body>

<div class="topBar">
    <div class="btn" id="messageButton">M</div>
    <br style="clear: both"/>
</div>

<div id="content"></div>
<div>
    <span id="status">Connecting...</span>
    GroupId :<input type="text" id="groupid" value="<?php echo $groupId;?>">
    <input type="text" id="role" value="student"/>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
</script>
<script src="./client.js"></script>
<script src="./node_modules/handlebars/dist/handlebars.js"></script>
<script id="messageTemplate" type="text/x-handlebars-template">
    <div class="message">
        <b>Message</b>
        <h1>{{title}}</h1>
        <div class="body">
            {{body}}
        </div>
    </div>
</script>
<script id="messageBoxTemplate" type="text/x-handlebars-template">
    <div class="messageBox" id="messageBox">
        Beskeder
        <div class="messagesContainer"></div>
    </div>
</script>
<script>

    let MessageBox = function() {
        const messageButton = $('#messageButton');
        setButtonClickEvent();
        let messages = [];
        var messageTemplate = Handlebars.compile($('#messageTemplate').html());

        var messageBoxTemplate = Handlebars.compile($('#messageBoxTemplate').html());
        $('body').append(messageBoxTemplate({title:'Effer', body:'Was here'}));

        function markUnread() {
            messageButton.addClass('unread');
        }

        function markRead() {
            messageButton.removeClass('unread');
        }

        function onMessageCallback(messageObject) {
            console.log('MESSAGE ARRIVED', messageObject);
            messages.push(messageObject);
            markUnread();
        }

        function setButtonClickEvent() {
            messageButton.on('click', function(e) {

                showMessages();
               console.log('i',messageButton);
               console.log('e',e);
            });
        }

        function showMessages() {
            $('.messageBox').addClass('show');
            let messagesContainer = $('.messagesContainer');
            messagesContainer.html('');

            for(var i=0; i<messages.length; i++) {
                addMessage(messages[i]);
            }
            markRead();
        }

        function addMessage(messageObject) {
            let messagesContainer = $('.messagesContainer');
            let template = messageTemplate({title: messageObject.time, body: messageObject.text});
            console.log('add message', template);
            console.log('container', $('.messagesContainer'));
            messagesContainer.append(template);
        }

        return {
            markUnread: markUnread,
            markRead: markRead,
            onMessageCallback: onMessageCallback
        }

    }


    var messageBox = new MessageBox();

    var client = new Client();
    client.onMessage(messageBox.onMessageCallback);



</script>
</body>
</html>
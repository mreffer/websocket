let Client = function() {
    "use strict";
    // for better performance - to avoid searching in DOM
    var content = $('#content');
    var input = $('#input');
    var status = $('#status');
    var groupId = $('#groupid').val();
    var role = $('#role').val();
    var onMessageEvent = null;

    // my color assigned by the server
    var myColor = false;
    // my name sent to the server
    var myName = false;
    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;
    // if browser doesn't support WebSocket, just show
    // some notification and exit
    if (!window.WebSocket) {
        content.html($('<p>',
            { text:'Sorry, but your browser doesn\'t support WebSocket.'}
        ));
        input.hide();
        $('span').hide();
        return;
    }
    // open connection
    var connection = new WebSocket('ws://127.0.0.1:1338');
    connection.onopen = function () {
        // first we want users to enter their names


        if (role == 'teacher') {
            input.removeAttr('disabled');
        }
        /*;
        status.text('Choose name:');*/
        connection.send(JSON.stringify({'messageType': 'initialise', 'userGroupId': groupId, 'userId': 1, role: role}));
        console.log('ONOPEN()');
    };
    connection.onerror = function (error) {
        // just in there were some problems with connection...
        content.html($('<p>', {
            text: 'Sorry, but there\'s some problem with your '
                + 'connection or the server is down.'
        }));
    };
    // most important part - incoming messages
    connection.onmessage = function (message) {
        // try to parse JSON message. Because we know that the server
        // always returns JSON this should work without any problem but
        // we should make sure that the massage is not chunked or
        // otherwise damaged.

        try {
            var json = JSON.parse(message.data);
        } catch (e) {
            console.log('Invalid JSON: ', message.data);
            return;
        }

        console.log('incoming message:', json);
        // NOTE: if you're not sure about the JSON structure
        // check the server source code above
        // first response from the server with user's color
        if (json.type === 'message') { // it's a single message
            // let the user write another message
            input.removeAttr('disabled');
            addMessage(json, new Date(json.time));
        } else {
            console.log('Hmm..., I\'ve never seen JSON like this:', json);
        }
    };
    /**
     * Send message when user presses Enter key
     */
    input.keydown(function(e) {
        if (e.keyCode === 13) {
            var msg = $(this).val();
            if (!msg) {
                return;
            }


            let $receiverIds = $('#receivers').val();
            let $groupId = $('#groupid').val();
            console.log('GROUPOID', $groupId);


            // send the message as an ordinary text
            connection.send(JSON.stringify(
                {
                    'messageType': 'msg',
                    text: msg,
                    userGroupId: $groupId,
                    'userId': 1,
                    role: role,
                    receivers: $receiverIds
                }
                ));
            $(this).val('');
        }
    });
    /**
     * This method is optional. If the server wasn't able to
     * respond to the in 3 seconds then show some error message
     * to notify the user that something is wrong.
     */
    setInterval(function() {
        if (connection.readyState !== 1) {
            status.text('Error');
            input.attr('disabled', 'disabled').val(
                'Unable to communicate with the WebSocket server.');
        }
    }, 3000);
    /**
     * Add message to the chat window
     */
    function addMessage(messageObj, dt) {
        content.append('<p><span>' + messageObj.author + '</span> @ ' + (dt.getHours() < 10 ? '0'
                + dt.getHours() : dt.getHours()) + ':'
            + (dt.getMinutes() < 10
                ? '0' + dt.getMinutes() : dt.getMinutes())
            + ': ' + messageObj.text + '</p>');


        if (onMessageEvent !== null) {
            console.log('event type', onMessageEvent);
            onMessageEvent(messageObj);
        }
    }


    function onMessage(callback) {
        console.log('onMessage callback set', callback);
        onMessageEvent = callback;
    }

    return {
        onMessage: onMessage
    }
};

